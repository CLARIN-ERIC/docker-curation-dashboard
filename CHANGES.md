# version 7.1.2_3.1.0
- upgrading to curation-dashboard 7.1.2

# version 7.1.1_3.1.0
- upgrading to curation-dashboard 7.1.1
- upgrading base image
